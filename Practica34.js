
//callback
//  const getMenuById = (id, callback) => {
//  const miercoles = [
//    {
//     id: 1,
//     almuerzo: 'mani',
//     segundo: 'sillpancho',
//     },

//     {
//     id: 2,
//     almuerzo: 'zapallo',
//     segundo: 'milanesa',
//     },

//     {
//       id: 3,
//       almuerzo: 'fideo',
//       segundo: 'pique',
//     },

//     {
//       id: 4,
//       almuerzo: 'lawa',
//       segundo: 'pollo',
//     },
//  ]

//  setTimeout(()=> { callback(miercoles); }, 1500); };
//  getMenuById(1, (miercoles) => { console.log(miercoles); })
 

//promesa

const viajes = [
  {
    id: 1,
    lugar: 'salar de uyuni'
  },
  {
    id: 2,
    lugar: 'samaipata'
  },
  {
    id: 3,
    lugar: 'toro toro'
  },
  {
    id: 4,
    lugar: 'lago titicaca'
  },

  {
    id: 5,
    lugar: 'tropico'
  },
];

const precios =[
  {
    id:1,
    precio: 1200
  },
  {
    id: 2,
    precio: 850
  },

  {
    id:3,
    precio: 550
  },

  {
    id: 4,
    precio: 600
  },

  {
    id:5,
    precio: 450
  }
];

getViaje(id)
  .then(empleado => {

    getSalario(id)
      .then(salario => {
        console.log('El empleado:', empleado, 'tiene un salario de: ', salario);
      })
      .catch(error => {
        //Codigo a realizar cuando se rechaza la promesa
        console.log(error);
      });;
  })
  .catch(error => {
    //Codigo a realizar cuando se rechaza la promesa
    console.log(error);
  });


let lugar;

getViaje(id)
.the(viaje => {
  console.log(viaje);
  lugar = viaje.lugar;
  return getViaje( );
})

.then(precio => {
  console.log('el viaje a:', lugar, 'tienen un costo de:', precio['precios']);
})

//async y await

// const getCotizacion = async (id) => {
// try{
//   const viaje = await getViaje(id);
//   const precio = await getPrecio (id);
//   return `el viaje: ${viaje.lugar} tiene el costo de ${precio['precio']}`
// }
// catch(ex){
//   throw ex;
// }
// };

// getCotizacion(id)
// .then(msg = console.log(msg))
// .catch(err => console.log(err));
